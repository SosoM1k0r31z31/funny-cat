import sqlite3
from sqlite3 import Error
# if you decide to change the file name you will have to update the code accordingly
connection = sqlite3.connect("msgarchive.sqlite")

with connection:
    connection.execute("""
        CREATE TABLE ARCHIVE (
            id TEXT not null,
            author TEXT not null,
            content TEXT not null,
            clean_content TEXT not null,
            jump_url TEXT not null,
            attachment_url TEXT,
            date TEXT not null,
            date_edited TEXT,
            hash TEXT not null,
            uuid TEXT not null
            unique (id)
        );
    """)
