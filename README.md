
# Funny cat

![preview 1](https://media.discordapp.net/attachments/863713323756814398/875428770692079636/output875428755991068692.gif)


See bot.py for dependencies (imports)

[Need help with commands?](https://sosom1k0r31z31.bitbucket.io/)

# Future features

None

# Useful resources

[Discord.py Documentation](https://discordpy.readthedocs.io/en/stable/)

# Self-hosting

If you want to self host this bot, keep in mind that some features won't work. Particularly ones that use Sqlite databases.

I will be including some scripts that generate the Sqlite files at a later date.

Make sure you also put your own API keys. (i will also be organising those later)

Powered by Python and Debian